# Lab2124 Handbook

At the moment, this documentation site describes how to write a Python package.
In the future, other material may be added.

:::{toctree}
:maxdepth: 1
:caption: "Contents:"

python_packaging_guide
style_guides
playbook_design_document_howto
playbook_git_commit_howto
design_document_addcharnoise

:::

# Search

- {ref}`search`
