# Playbook: How to write a design document

Created: 2023-03-29

Last updated: 2023-03-29

## Abstract

TBD

## Background

TBD

Design documents need not be limited to technical matters.
Design documents can and often do describe proposals that concern the operation or governance of a community.
Some design documents describe a community's values or principles.
Examples of the latter include Go's [A Code of Conduct for the Go community](https://github.com/golang/proposal/blob/master/design/13073-code-of-conduct.md) and the IETF's [Pervasive Monitoring Is an Attack](https://www.rfc-editor.org/rfc/rfc7258).

## Instructions

TBD

## Further reading

### Examples of design documents

#### Go programming language design documents

These examples are chosen essentially at random.

- [Proposal: Ignore tags in struct type conversions](https://github.com/golang/proposal/blob/master/design/16085-conversions-ignore-tags.md)
- [Proposal: Natural XML](https://github.com/golang/proposal/blob/master/design/13504-natural-xml.md)
- [Proposal: `-json` flag in `go test`](https://raw.githubusercontent.com/golang/proposal/master/design/2981-go-test-json.md)
- [Proposal: Monotonic Elapsed Time Measurements in Go](https://github.com/golang/proposal/blob/master/design/12914-monotonic.md)
- [Proposal: A Code of Conduct for the Go community](https://github.com/golang/proposal/blob/master/design/13073-code-of-conduct.md)


### Python programming language design documents

TBD
