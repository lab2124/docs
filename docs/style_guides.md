# Style Guides

This page contains style guides for languages other than Python.

- Bash: Follow Google's [Shell Style Guide](https://google.github.io/styleguide/shellguide.html)
