# Playbook: How to craft a git commit

Created: 2023-04-12

Last updated: 2023-04-12

## Abstract

TBD

## Background

TBD

## Instructions

TBD

## Further reading

- [Tips for a disciplined git workflow](https://drewdevault.com/2019/02/25/Using-git-with-discipline.html)
