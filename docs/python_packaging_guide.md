# Python Packaging Guide

Most of the good Python packaging guides will tell you to do similar things. They will all tell you to

1. use a modern build system that follows PEP 621,
1. test your code using `pytest`,
1. use `black` to format your code, and
1. check your code for common errors using [ruff](https://github.com/charliermarsh/ruff) or a similar tool.

Some guides will tell you do much more. For example, some will insist
that you exhaustively annotate your variables and function signatures with types.

A demanding Python packaging guide is available from Scikit-HEP, [Developer Information](https://scikit-hep.org/developer).
Our packaging guide uses Scikit-HEP's recommendations as a starting point.
We keep the essential recommendations and ignore others.

## Creating a Python Package

*This section is a work-in-progress.*

For now, make a copy of <https://www.codeberg.org/lab2124/cleannyt> and use it as a template for a new package.

The starting point for `cleannyt` was Scikit-HEP's template, <https://github.com/scikit-hep/cookie>.
Numerous checks and features were removed. Some of the goals that informed decisions about what to keep/cut/change are:

- Support Python 3.10, ignore earlier versions
- Use `hatch` for pure Python packages
- Do not worry about type checking

<!-- important but inside baseball

- Use [0BSD](https://choosealicense.com/licenses/0bsd/) as the license

-->
