# Dockerfile that builds Sphinx documentation and serves it.
#
# Intended for use with dokku. Tip: keep in sync with a repository by deploying
# every 6 hours using a cron task. See dokku's recommendations on scheduled
# cron tasks:
# https://dokku.com/docs/processes/scheduled-cron-tasks/#general-cron-recommendations

FROM python:3.11-alpine

RUN apk add make py3-yaml py3-sphinx py3-myst-parser

COPY . /app
WORKDIR /app

RUN python3 -m pip install -r requirements.txt
RUN make html

FROM nginx:alpine

COPY --from=0 /app/build/html /usr/share/nginx/html
